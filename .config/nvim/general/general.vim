set encoding=utf-8
set clipboard+=unnamedplus
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set mouse=a
set number relativenumber
set ignorecase
set cursorline
set cursorcolumn
set wildmenu
set wildmode=longest:full
syntax on
filetype plugin indent on
set nocompatible
set scrolloff=999

" Theme
"let g:gruvbox_italic=1
colorscheme dracula
set termguicolors
hi Normal guibg=NONE ctermbg=NONE

" NERDTree
let NERDTreeShowHidden=1

" Airline
let g:airline_theme='dracula'
let g:airline_powerline_fonts=1

" VimWiki
"let g:vimwiki_list = [{'path': '~/Documents/Vimwiki/', 'auto_diary_index': 1}]

" Disable coc.nvim suggestions in vimwiki files
"autocmd FileType vimwiki let b:coc_suggest_disable=1

" coc.nvim
let g:coc_global_extensions = ['coc-css', 'coc-html', 'coc-json', 'coc-markdownlint', 'coc-pairs', 'coc-tsserver', 'coc-word']
command! -nargs=0 Format :call CocActionAsync('format')

" This section is useful for Vim/Neovim to remember the last place where it left
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

