" Leader key
let mapleader = " "

" Switch between different windows by their direction
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

" Copy and Paste
vnoremap <C-c> "+y
vnoremap <C-x> "+c
inoremap <C-v> <C-r><C-o>+

" Filetype Specifics
autocmd FileType python nnoremap <buffer> <silent><leader><C-v> :vsplit term://python3 %<CR>
autocmd FileType python nnoremap <buffer> <silent><leader><C-h> :split term://python3 %<CR>

" Toggle wrap/nowrap
nnoremap <silent><leader><C-w> :set nowrap!<CR>

" NERDTree
nnoremap <silent><C-n> :NERDTreeToggle<CR>

" NERDCommenter
noremap <C-p> 0<Plug>NERDCommenterToggle<CR><BS>
inoremap <C-p> <C-o>0<C-o><Plug>NERDCommenterToggle<CR><BS><C-o>A

" coc.nvim
inoremap <silent><expr> <C-Space> coc#refresh()
nnoremap <silent><leader>f :Format<CR>
inoremap <silent><expr> <CR> coc#pum#visible() && coc#pum#info()['index'] != -1 ? coc#pum#confirm() : "\<C-g>u\<CR>"

" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()

" VimWiki
"nmap <C-Up> <Plug>VimwikiDiaryNextDay
"nmap <C-Down> <Plug>VimwikiDiaryPrevDay

