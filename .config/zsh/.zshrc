# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Imports
source "$ZDOTDIR/zsh-functions"
zsh_add_file "zsh-aliases"

# Some settings
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.local/state/zsh/history
setopt autocd globdots histignoredups
bindkey -e
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
autoload -Uz compinit
zstyle ':completion:*' menu select

# Prompt
zsh_add_file "zsh-prompt"

# Backup & restore SSH keys
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi

# Plugins
zsh_add_plugin "zsh-users/zsh-completions"
zsh_add_plugin "zsh-users/zsh-autosuggestions"
zsh_add_plugin "zsh-users/zsh-syntax-highlighting"
source /usr/share/doc/pkgfile/command-not-found.zsh

compinit

pfetch

