#!/bin/python

import os

username = os.getlogin()

try:
    with open(f"/home/{username}/.local/share/weather/icon", "r") as file:
        print(file.readline())

except FileNotFoundError:
    print("Invalid Path!")
