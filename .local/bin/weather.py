#!/bin/python

from time import time
import os
import requests
import sys

CITY = "Put your city or district name here"
UNITS = "metric"
UNIT_STRING = "°C"
API_KEY = "Put your API key here"


USERNAME = os.getlogin()


def argument_city():
    try:
        global CITY
        CITY = sys.argv[1]
    except IndexError:
        pass


def get_coordinates():
    URL = f"https://api.openweathermap.org/geo/1.0/direct?q={CITY}&limit=1&appid={API_KEY}"
    request = requests.get(URL)
    global LATITUDE
    global LONGITUDE

    try:
        if request.status_code == 200:
            LATITUDE = request.json()[0]["lat"]
            LONGITUDE = request.json()[0]["lon"]

    except (ValueError, IOError):
        print("Error!")
        exit()

    except NameError:
        print("Invalid API Key!")
        exit()

    except IndexError:
        print("Unknown City or District Name!")
        exit()


def get_weather_data():
    URL = f"https://api.openweathermap.org/data/2.5/weather?lat={LATITUDE}&lon={LONGITUDE}&units={UNITS}&appid={API_KEY}"
    request = requests.get(URL)

    if request.status_code == 200:
        global SUNRISE
        global SUNSET
        global DESCRIPTION
        DESCRIPTION = request.json()["weather"][0]["description"].title()
        SUNRISE = request.json()["sys"]["sunrise"]
        SUNSET = request.json()["sys"]["sunset"]
        CURRENT_TEMP = str(round(request.json()["main"]["temp"]))
        FEELS_LIKE = request.json()["main"]["feels_like"]

        print(DESCRIPTION, str(CURRENT_TEMP), UNIT_STRING)


def find_is_day():
    current_time = time()
    global IS_DAY

    if ((current_time > SUNRISE) and (current_time > SUNSET)) or (
        (current_time < SUNRISE) and (current_time < SUNSET)
    ):
        IS_DAY = False
    else:
        IS_DAY = True


def icon_picker():
    global ICON

    if IS_DAY == True:
        if DESCRIPTION == "Clear Sky":
            ICON = ""
        elif (
            DESCRIPTION == "Few Clouds"
            or DESCRIPTION == "Scattered Clouds"
            or DESCRIPTION == "Broken Clouds"
            or DESCRIPTION == "Overcast Clouds"
        ):
            ICON = ""
        elif (
            DESCRIPTION == "Shower Rain"
            or DESCRIPTION == "Light Rain"
            or DESCRIPTION == "Light Intensity Shower Rain"
            or DESCRIPTION == "Light Intensity Drizzle"
            or DESCRIPTION == "Heavy Intensity Drizzle"
            or DESCRIPTION == "Light Shower Sleet"
            or DESCRIPTION == "Thunderstorm With Light Rain"
        ):
            ICON = ""
        elif DESCRIPTION == "Rain":
            ICON = ""
        elif DESCRIPTION == "Thunderstorm":
            ICON = ""
        elif (
            DESCRIPTION == "Snow"
            or DESCRIPTION == "Light Snow"
            or DESCRIPTION == "Shower Snow"
            or DESCRIPTION == "Light Shower Snow"
        ):
            ICON = ""
        elif DESCRIPTION == "Mist" or DESCRIPTION == "Fog":
            ICON = ""

    else:
        if DESCRIPTION == "Clear Sky":
            ICON = ""
        elif (
            DESCRIPTION == "Few Clouds"
            or DESCRIPTION == "Scattered Clouds"
            or DESCRIPTION == "Broken Clouds"
            or DESCRIPTION == "Overcast Clouds"
        ):
            ICON = ""
        elif (
            DESCRIPTION == "Shower Rain"
            or DESCRIPTION == "Light Rain"
            or DESCRIPTION == "Light Intensity Shower Rain"
            or DESCRIPTION == "Light Intensity Drizzle"
            or DESCRIPTION == "Heavy Intensity Drizzle"
            or DESCRIPTION == "Light Shower Sleet"
            or DESCRIPTION == "Thunderstorm With Light Rain"
        ):
            ICON = ""
        elif DESCRIPTION == "Rain":
            ICON = ""
        elif DESCRIPTION == "Thunderstorm":
            ICON = ""
        elif (
            DESCRIPTION == "Snow"
            or DESCRIPTION == "Light Snow"
            or DESCRIPTION == "Shower Snow"
            or DESCRIPTION == "Light Shower Snow"
        ):
            ICON = ""
        elif DESCRIPTION == "Mist" or DESCRIPTION == "Fog":
            ICON = ""


def icon_saver():
    directory_exists = os.path.isdir(f"/home/{USERNAME}/.local/share/weather")

    if directory_exists == False:
        os.makedirs(f"/home/{USERNAME}/.local/share/weather")

    with open(f"/home/{USERNAME}/.local/share/weather/icon", "w") as file:
        file.write(ICON)


def main():
    argument_city()
    get_coordinates()
    get_weather_data()
    find_is_day()
    icon_picker()
    icon_saver()


if __name__ == "__main__":
    main()
