# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Prompt
PS1='\[\e[1;33m\][\u]\[\e[1;34m\][\W]\[\e[0m\] \[\e[1;31m\]->\[\e[0m\] '

# History file
export HISTFILE="$XDG_STATE_HOME/bash/history"

# Aliases
source "$HOME/.config/zsh/zsh-aliases"

# pkgfile
source /usr/share/doc/pkgfile/command-not-found.bash

# Backup & restore SSH keys
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi

pfetch

