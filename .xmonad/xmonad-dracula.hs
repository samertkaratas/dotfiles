-- IMPORTS --

import qualified Data.Map as M
import System.Exit ( ExitCode(ExitSuccess), exitWith )
import XMonad
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.ManageDocks
  ( avoidStruts,
    docks,
  )
import XMonad.Hooks.ManageHelpers
  ( composeOne,
    doCenterFloat,
    doFullFloat,
    isDialog,
    isFullscreen,
    (-?>),
  )
import XMonad.Hooks.RefocusLast
  ( isFloat,
    refocusLastLayoutHook,
    refocusLastWhen,
  )
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Hooks.StatusBar
  ( StatusBarConfig,
    dynamicSBs,
    statusBarPropTo,
  )
import XMonad.Hooks.StatusBar.PP
  ( PP (ppCurrent, ppOrder, ppSep, ppTitle, ppVisible, ppWsSep),
    dynamicLogWithPP,
    filterOutWsPP,
    shorten,
    xmobarColor,
  )
import XMonad.Layout.IndependentScreens
  ( marshallPP,
    onCurrentScreen,
    withScreens,
    workspaces',
  )
import XMonad.Layout.MultiToggle
  ( EOT (EOT),
    Toggle (Toggle),
    mkToggle,
    (??),
  )
import XMonad.Layout.MultiToggle.Instances
  ( StdTransformers (NBFULL),
  )
import XMonad.Layout.NoBorders (Ambiguity (OnlyScreenFloat), lessBorders)
import XMonad.Layout.Spacing (spacingWithEdge)
import XMonad.Layout.TrackFloating (trackFloating)
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
  ( NamedScratchpad (NS),
    customFloating,
    defaultFloating,
    namedScratchpadAction,
    namedScratchpadManageHook,
    scratchpadWorkspaceTag,
  )
import XMonad.Util.SpawnOnce (spawnOnce)

------------------------------------------------------------------------
-- SOME SETTINGS --

-- Default terminal emulator
myTerminal :: String
myTerminal = "alacritty"

-- Whether or not focusing window is determined by also a mouse movement, not by only a mouse click
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether or not clicking on an unfocused window also passes the click to the window as well as focusing on it
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Default border width
myBorderWidth :: Dimension
myBorderWidth = 1

-- Default mod key
myModMask :: KeyMask
myModMask = mod4Mask

-- Workspace names as a list
myWorkspaces :: [String]
myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]

------------------------------------------------------------------------
-- COLORS --

-- Border color for unfocused window
myNormalBorderColor :: String
myNormalBorderColor = "#282A36"

-- Border color for focused window
myFocusedBorderColor :: String
myFocusedBorderColor = "#F8F8F2"

-- Foreground tag color for active and closed workspaces
activeClosed :: String
activeClosed = "#F8F8F2"

-- Foreground tag color for the active and open workspace
activeOpenForeground :: String
activeOpenForeground = "#282A36"

-- Background tag color for the active and open workspace
activeOpenBackground :: String
activeOpenBackground = "#FFCA9D"

-- Foreground title color
titleForeground :: String
titleForeground = "#FFCA9D"

------------------------------------------------------------------------
-- KEYBINDINGS --

myEZKeys =
  [ -- Window manager keybindings
    ("M-<Return>", spawn myTerminal),
    ("M-c", kill),
    ("M-<Tab>", windows W.focusDown),
    ("M-j", windows W.focusDown),
    ("M-k", windows W.focusUp),
    ("M-S-j", windows W.swapDown),
    ("M-S-k", windows W.swapUp),
    ("M-h", sendMessage Shrink),
    ("M-l", sendMessage Expand),
    ("M-t", withFocused $ windows . W.sink),
    ("M-,", sendMessage (IncMasterN 1)),
    ("M-.", sendMessage (IncMasterN (-1))),
    ("M-m", sendMessage $ Toggle NBFULL),
    ("M-S-q", io $ exitWith ExitSuccess),
    ("M-S-r", spawn "xmonad-restart"),
    -- Fn keys
    ("<XF86AudioRaiseVolume>", spawn "sh -c 'pactl set-sink-mute @DEFAULT_SINK@ false ; pactl set-sink-volume @DEFAULT_SINK@ +5%'"),
    ("<XF86AudioLowerVolume>", spawn "sh -c 'pactl set-sink-mute @DEFAULT_SINK@ false ; pactl set-sink-volume @DEFAULT_SINK@ -5%'"),
    ("<XF86AudioMute>", spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle"),
    ("<XF86MonBrightnessUp>", spawn "xbacklight -inc 10"),
    ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 10"),
    -- Custom keybindings
    ("M-S-l", spawn "slock"),
    ("M-d t", spawn "dunstctl set-paused true"),
    ("M-d f", spawn "dunstctl set-paused false"),
    ("M-s p", spawn "playerctl -p firefox play-pause"),
    ("M-s <R>", spawn "playerctl -p firefox next"),
    ("M-s <L>", spawn "playerctl -p firefox previous"),
    ("M-f c", spawn "chromium"),
    ("M-f f", spawn "librewolf"),
    ("M-r", spawn "dmenu_run -p Run"),
    ("M-s s", spawn "pavucontrol"),
    ("<Print>", spawn "flameshot gui"),
    ("M-<Print>", spawn "flameshot screen"),
    ("M-p e", spawn "bemoji -n"),
    ("M-p t", spawn "dmenu-translate"),
    ("M-w M-s", spawn "dmenu-wallpaper select"),
    ("M-w M-r", spawn "dmenu-wallpaper random"),
    ("M-u f", spawn "pcmanfm"),
    ("M-S-f", namedScratchpadAction scratchpads "ranger"),
    ("M-S-<Return>", namedScratchpadAction scratchpads "alacritty"),
    ("M-S-c", namedScratchpadAction scratchpads "qalculate-gtk")
  ]

-- This section is for number keys on the keyboard to be used for switching workspaces
myKeys conf =
  let modm = modMask conf
   in M.fromList $
        [ ((m .|. modm, k), windows $ onCurrentScreen f i)
          | (i, k) <- zip (workspaces' conf) [xK_1 .. xK_9],
            (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
        ]

------------------------------------------------------------------------
-- MOUSE BINDINGS --

myMouseBindings (XConfig {XMonad.modMask = modm}) =
  M.fromList
    [ ((modm, button1), \w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster),
      ((modm, button2), \w -> focus w >> windows W.shiftMaster),
      ((modm, button3), \w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster)
    ]

------------------------------------------------------------------------
-- LAYOUTS --

myLayout = refocusLastLayoutHook . trackFloating $ myLayouts
  where
    myTallLayout = mkToggle (NBFULL ?? EOT) $ avoidStruts $ spacingWithEdge 8 $ lessBorders OnlyScreenFloat $ Tall numberOfMasterWindows incrementRatio defaultSizeRatio
    numberOfMasterWindows = 1
    incrementRatio = 1 / 100
    defaultSizeRatio = 1 / 2

    myLayouts = myTallLayout

------------------------------------------------------------------------
-- MANAGE HOOK --

myManageHook =
  composeAll
    [ className =? "zoom" --> doCenterFloat,
      className =? "Blueman-manager" --> doCenterFloat,
      className =? "Pavucontrol" --> doCenterFloat,
      className =? "LibreWolf" <&&> title =? "Library" --> doCenterFloat,
      className =? "Nm-connection-editor" --> doCenterFloat,
      className =? "Steam" --> doCenterFloat,
      className =? "Nitrogen" --> doCenterFloat,
      className =? "Gcolor3" --> doCenterFloat,
      className =? "metadata" --> doCenterFloat,
      className =? "Qalculate-gtk" --> doCenterFloat,
      className =? "Gucharmap" --> doCenterFloat
    ]
    <+> namedScratchpadManageHook scratchpads
    <+> composeOne
      [ isDialog -?> doCenterFloat,
        isFullscreen -?> doFullFloat
      ]

------------------------------------------------------------------------
-- LOG HOOK --

myLogHook :: X ()
myLogHook = dynamicLogWithPP myXMobarPP

------------------------------------------------------------------------
-- EVENT HOOK --

myEventHook = refocusLastWhen isFloat

------------------------------------------------------------------------
-- XMOBAR STYLE --

myXMobarPP :: PP
myXMobarPP =
  def
    { ppCurrent = xmobarColor activeOpenForeground activeOpenBackground,
      ppVisible = xmobarColor activeOpenForeground activeOpenBackground,
      ppSep = "  ",
      ppWsSep = "",
      ppTitle = xmobarColor titleForeground "" . shorten 60,
      ppOrder = \(ws : _ : t : _) -> [ws, t]
    }

myFirstStatusBar :: StatusBarConfig
myFirstStatusBar = statusBarPropTo "_XMONAD_LOG_0" "xmobar -x 0 ~/.config/xmobar/xmobarrc0" $ pure (filterOutWsPP [scratchpadWorkspaceTag] (marshallPP (S 0) myXMobarPP))

mySecondStatusBar :: StatusBarConfig
mySecondStatusBar = statusBarPropTo "_XMONAD_LOG_1" "xmobar -x 1 ~/.config/xmobar/xmobarrc1" $ pure (filterOutWsPP [scratchpadWorkspaceTag] (marshallPP (S 1) myXMobarPP))

barSpawner :: ScreenId -> IO StatusBarConfig
barSpawner 0 = pure myFirstStatusBar
barSpawner 1 = pure mySecondStatusBar
barSpawner _ = mempty

------------------------------------------------------------------------
-- STARTUP APPLICATIONS --

myStartupHook :: X ()
myStartupHook = do
  spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
  spawnOnce "xset s off -dpms"
  spawnOnce "picom"
  spawnOnce "xsetroot -cursor_name left_ptr"
  spawnOnce "restore-wallpaper"
  spawnOnce "sleep 5 && tray0"
  spawnOnce "sleep 7 && blueman-applet"
  spawnOnce "sleep 7 && nm-applet"
  spawnOnce "dunst"
  spawnOnce "play-with-mpv"
  setWMName "LG3D"

------------------------------------------------------------------------
-- SCRATCHPADS --

-- To center a window, dimensions are ((1-x)/2) ((1-y)/2) (x) (y)
scratchpadDimensions :: ManageHook
scratchpadDimensions = customFloating $ W.RationalRect (23 / 96) (2 / 9) (25 / 48) (5 / 9)

scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "ranger" "alacritty --class scratchpadRanger,scratchpadRanger --title Ranger -e ranger" (className =? "scratchpadRanger") scratchpadDimensions,
    NS "alacritty" "alacritty --class scratchpadTerminal,scratchpadTerminal" (className =? "scratchpadTerminal") scratchpadDimensions,
    NS "qalculate-gtk" "qalculate-gtk" (className =? "Qalculate-gtk") defaultFloating
  ]

------------------------------------------------------------------------
-- MAIN FUNCTION --

main :: IO ()
main = do
  xmonad $
    dynamicSBs barSpawner . ewmhFullscreen . ewmh . docks $
      def
        { terminal = myTerminal,
          focusFollowsMouse = myFocusFollowsMouse,
          clickJustFocuses = myClickJustFocuses,
          borderWidth = myBorderWidth,
          modMask = myModMask,
          workspaces = withScreens 2 myWorkspaces,
          normalBorderColor = myNormalBorderColor,
          focusedBorderColor = myFocusedBorderColor,
          keys = myKeys,
          mouseBindings = myMouseBindings,
          layoutHook = myLayout,
          manageHook = myManageHook,
          logHook = myLogHook,
          handleEventHook = myEventHook,
          startupHook = myStartupHook
        }
        `additionalKeysP` myEZKeys
